@extends('layouts.admin')
@section('content')

    <section class="content-header">
        <h1>
            <i class='fa fa-users'></i> Edit Data Pendaftar &nbsp;&nbsp;
        </h1>

        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Halaman Awal</a></li>
            <li class="active">Edit Data Pendaftar</li>
        </ol>
    </section>

    <section id='content_section' class="content">

    <div class="row" style="margin-bottom: 10px">
        <div class="col-sm-6">
            <div style="padding-top: 5px">
                <p><a title='Return' href="{{url('/admin')}}">
                    <i class='fa fa-chevron-circle-left '></i>&nbsp; Kembali ke halaman pendaftar</a>
                </p>
            </div>
        </div>
        <div class="col-sm-6">
            <div align="right"></div>
        </div>
    </div>

    <div class="row">
        <form action="{{ url('simpan_data_pendaftar') }}" method="post">
          @csrf
        <div class="data_nama"></div>
        <div class="col-md-12">
          @include('form.data_peserta_didik')
        </div>
        <div class="col-md-12">
          @include('form.data_keluarga')
          @include('form.data_lain')
          <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-send"></i> Simpan</button>
        </div>
        </form>
    </div>

    </div><!--END AUTO MARGIN-->
    </section>
@endsection
