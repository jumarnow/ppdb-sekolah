<table>
    <tr>
        <th>no</th>
        <th>nama_lengkap</th>
        <th>jenis_kelamin</th>
        <th>nisn</th>
        <th>nik</th>
        <th>nama_sekolah_asal</th>
        <th>alamat_sekolah_asal</th>
        <th>tempat_lahir</th>
        <th>tanggal_lahir</th>
        <th>agama</th>
        <th>provinsi</th>
        <th>kabupaten</th>
        <th>kecamatan</th>
        <th>kelurahan</th>
        <th>alamat</th>
        <th>tranportasi</th>
        <th>no_hp</th>
        <th>jenis_bantuan</th>
        <th>no_bantuan</th>
        <th>lintang</th>
        <th>bujur</th>
        <th>pendaftaran</th>
        <th>daftar_ulang</th>
        <th>nama_ayah</th>
        <th>pekerjaan_ayah</th>
        <th>pendidikan_ayah</th>
        <th>penghasilan_ayah</th>
        <th>nama_ibu</th>
        <th>pekerjaan_ibu</th>
        <th>pendidikan_ibu</th>
        <th>penghasilan_ibu</th>
        <th>nama_pondok</th>
        <th>tinggi_badan</th>
        <th>berat_badan</th>
        <th>jarak_kesekolah</th>
        <th>anak_ke</th>
        <th>jumlah_saudara</th>
    </tr>
    @foreach ($siswa as $k=>$i)
    <tr>
        <th>{{ $k+1 }}</th>
        <th>{{ $i->nama_lengkap }}</th>
        <th>{{ $i->jenis_kelamin }}</th>
        <th>'{{ $i->nisn }}</th>
        <th>'{{ $i->nik }}</th>
        <th>{{ $i->nama_sekolah_asal }}</th>
        <th>{{ $i->alamat_sekolah_asal }}</th>
        <th>{{ $i->tempat_lahir }}</th>
        <th>{{ $i->tanggal_lahir }}</th>
        <th>{{ $i->agama }}</th>
        <th>{{ $i->provinsi }}</th>
        <th>{{ $i->kabupaten }}</th>
        <th>{{ $i->kecamatan }}</th>
        <th>{{ $i->kelurahan }}</th>
        <th>{{ $i->alamat }}</th>
        <th>{{ $i->tranportasi }}</th>
        <th>{{ $i->no_hp }}</th>
        <th>{{ $i->jenis_bantuan }}</th>
        <th>{{ $i->no_bantuan }}</th>
        <th>{{ $i->lintang }}</th>
        <th>{{ $i->bujur }}</th>
        <th>{{ $i->pendaftaran }}</th>
        <th>{{ $i->daftar_ulang }}</th>
        <th>{{ $i->keluarga->nama_ayah ?? ""}}</th>
        <th>{{ $i->keluarga->pekerjaan_ayah ?? ""}}</th>
        <th>{{ $i->keluarga->pendidikan_ayah ?? ""}}</th>
        <th>{{ $i->keluarga->penghasilan_ayah ?? ""}}</th>
        <th>{{ $i->keluarga->nama_ibu ?? ""}}</th>
        <th>{{ $i->keluarga->pekerjaan_ibu ?? ""}}</th>
        <th>{{ $i->keluarga->pendidikan_ibu ?? ""}}</th>
        <th>{{ $i->keluarga->penghasilan_ibu ?? ""}}</th>
        <th>{{ $i->lain->nama_pondok ?? ""}}</th>
        <th>{{ $i->lain->tinggi_badan ?? ""}}</th>
        <th>{{ $i->lain->berat_badan ?? ""}}</th>
        <th>{{ $i->lain->jarak_kesekolah ?? ""}}</th>
        <th>{{ $i->lain->anak_ke ?? ""}}</th>
        <th>{{ $i->lain->jumlah_saudara ?? ""}}</th>
    </tr>
    @endforeach
</table>