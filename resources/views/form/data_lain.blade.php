<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Data Lain</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    {{-- <form role="form" action="{{ route('data_keluarga.store') }}"> --}}
        <div class="form-horizontal">
        <div class="box-body">
        <div class="form-group">
            <label for="" class="col-sm-2 text-right">Rencana Pondok Pesantren</label> <br>
            <div class="col-sm-10">
                <input type="radio" name="pondok" value="ya" @if(isset($siswa) && $siswa->lain->nama_pondok != '') checked @endif> Ya
                <input type="radio" name="pondok" value="tidak" @if(isset($siswa) && $siswa->lain->nama_pondok == '') checked @endif> Tidak
                <select name="nama_pondok" class="nama_pondok form-control" id="">
                    <option value="">Pilih Pondok</option>
                    <option @if(isset($siswa) && $siswa->lain->nama_pondok == 'Futuhiyyah') selected @endif>Futuhiyyah</option>
                    <option @if(isset($siswa) && $siswa->lain->nama_pondok == 'Al Mubarok') selected @endif>Al Mubarok</option>
                    <option @if(isset($siswa) && $siswa->lain->nama_pondok == "Darussa'adah") selected @endif>Darussa'adah</option>
                    <option @if(isset($siswa) && $siswa->lain->nama_pondok == 'Al Badriyah') selected @endif>Al Badriyah</option>
                    <option @if(isset($siswa) && $siswa->lain->nama_pondok == 'Al Amin') selected @endif>Al Amin</option>
                    <option @if(isset($siswa) && $siswa->lain->nama_pondok == 'Nurul Burhani 1') selected @endif>Nurul Burhani 1</option>
                    <option @if(isset($siswa) && $siswa->lain->nama_pondok == 'Nurul Burhani 2') selected @endif>Nurul Burhani 2</option>
                    <option @if(isset($siswa) && $siswa->lain->nama_pondok == "Darul Ma'wa") selected @endif>Darul Ma'wa</option>
                    <option @if(isset($siswa) && $siswa->lain->nama_pondok == 'KH Murodi') selected @endif>KH Murodi</option>
                    <option @if(isset($siswa) && $siswa->lain->nama_pondok == 'Al Anwar') selected @endif>Al Anwar</option>
                    <option @if(isset($siswa) && $siswa->lain->nama_pondok == 'Al Imdad') selected @endif>Al Imdad</option>
                    <option @if(isset($siswa) && $siswa->lain->nama_pondok == 'Lainnya') selected @endif>Lainnya</option>
                </select>
                <div class="pondok_lain"></div>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 text-right">Tinggi Badan (cm), Berat Badan (kg)</label>
            <div class="col-sm-10">
            <div class="form-inline">
                <input type="number" name="tinggi_badan" class="form-control" value="{{ isset($siswa) ? $siswa->lain->tinggi_badan : '' }}" placeholder="Tinggi Badan">
                <input type="number" name="berat_badan" class="form-control" value="{{ isset($siswa) ? $siswa->lain->berat_badan : '' }}" placeholder="Berat Badan">
            </div>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 text-right">Jarak ke sekolah (km) </label>
            <div class="col-sm-10">
            <input type="number" class="form-control" name="jarak_kesekolah" value="{{ isset($siswa) ? $siswa->lain->jarak_kesekolah : '' }}">
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 text-right">Waktu ke sekolah (menit) </label>
            <div class="col-sm-10">
            <input type="number" class="form-control" name="waktu_kesekolah" value="{{ isset($siswa) ? $siswa->lain->waktu_kesekolah : '' }}">
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 text-right">Anak Ke, Jumlah Saudara <span style="color: red">*</span></label>
            <div class="col-sm-10">
            <div class="form-inline">
                <input type="number" required name="anak_ke" class="form-control" value="{{ isset($siswa) ? $siswa->lain->anak_ke : '' }}" placeholder="Anak Ke">
                <input type="number" required name="jumlah_saudara" class="form-control" value="{{ isset($siswa) ? $siswa->lain->jumlah_saudara : '' }}" placeholder="Jumlah Saudara">
            </div>
            </div>
        </div>

        </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
        {{-- <button type="submit" class="btn btn-primary pull-right"><i class="fa-send"></i> Kirim Formulir</button> --}}
        </div>
    {{-- </form> --}}
</div>
