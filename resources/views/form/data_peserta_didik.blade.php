<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Data Peserta Didik</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    {{-- <form class="form-horizontal" action="{{ route('data_peserta_didik.store') }}"> --}}
        <div class="form-horizontal">
        <div class="box-body">
        <div class="form-group">
            <label for="" class="col-sm-2 text-right">Nama Lengkap <span style="color: red">*</span></label>
            <div class="col-sm-10">
                <input type="text" required class="form-control" name="nama_lengkap" value="{{ isset($siswa) ? $siswa->nama_lengkap : '' }}">
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 text-right">Jenis Kelamin <span style="color: red">*</span></label> <br>
            <div class="col-sm-10">
                <input type="radio" name="jenis_kelamin" value="L" @if(isset($siswa) && $siswa->jenis_kelamin == 'L') checked @endif> Laki-laki
                <input type="radio" name="jenis_kelamin" value="P" @if(isset($siswa) && $siswa->jenis_kelamin == 'P') checked @endif> Perempuan
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 text-right">NISN <span style="color: red">*</span></label>
            <div class="col-sm-10">
            <input type="number" required @if(request()->is('edi*')) readonly @endif name="nisn" class="panjang_nisn form-control" id="" value="{{ isset($siswa) ? $siswa->nisn : '' }}">
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 text-right">NIK <span style="color: red">*</span></label>
            <div class="col-sm-10">
            <input type="number" required name="nik" class="panjang_nik form-control" id="" value="{{ isset($siswa) ? $siswa->nik : '' }}">
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 text-right">Nama Sekolah Asal <span style="color: red">*</span></label>
            <div class="col-sm-10">
            <input type="text" required name="nama_sekolah_asal" class="form-control" id="" value="{{ isset($siswa) ? $siswa->nama_sekolah_asal : '' }}">
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 text-right">Alamat Sekolah Asal <span style="color: red">*</span></label>
            <div class="col-sm-10">
            <textarea required name="alamat_sekolah_asal" class="form-control" id="" cols="30" rows="3">{{ isset($siswa) ? $siswa->alamat_sekolah_asal : '' }}</textarea>
            {{-- <input type="number" required name="alamat_sekolah_asal" class="form-control" id="" placeholder=""> --}}
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 text-right">Tempat Lahir <span style="color: red">*</span></label>
            <div class="col-sm-10">
            <div class="form-inline">
                <input type="text" required name="tempat_lahir" class="form-control" id="" value="{{ isset($siswa) ? $siswa->tempat_lahir : '' }}">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 text-right">Tanggal Lahir <span style="color: red">*</span></label>
            <div class="col-sm-10">
            <div class="form-inline">
                <input type="date" required name="tanggal_lahir" class="form-control" id="" value="{{ isset($siswa) ? $siswa->tanggal_lahir : '' }}">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 text-right">Agama <span style="color: red">*</span></label>
            <div class="col-sm-10">
            <select name="agama" required class="form-control" id="">
                <option selected>Islam</option>
                <option>kristen</option>
                <option>Budha</option>
                <option>Khatolik</option>
            </select>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 text-right">Provinsi <span style="color: red">*</span></label>
            <div class="col-sm-10">
            @if (isset($siswa))
            <input type="text" required name="provinsi" class="form-control" id="" value="{{ $siswa->provinsi }}">
            @else
            <select name="" required id="provinsi" class="form-control select2">
                <option value="">Pilih Provinsi</option>
            </select>
            @endif
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 text-right">Kabupaten <span style="color: red">*</span></label>
            <div class="col-sm-10">
            @if (isset($siswa))
            <input type="text" required name="kabupaten" class="form-control" id="" value="{{ $siswa->kabupaten }}">
            @else
            <select name="" required id="kabupaten" class="form-control select2">
                <option value="">Pilih Kabupaten/Kota</option>
            </select>
            @endif
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 text-right">Kecamatan <span style="color: red">*</span></label>
            <div class="col-sm-10">
            @if (isset($siswa))
            <input type="text" required name="kecamatan" class="form-control" id="" value="{{ $siswa->kecamatan }}">
            @else
            <select name="" required id="kecamatan" class="form-control select2">
                <option value="">Pilih Kecamatan</option>
            </select>
            @endif
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 text-right">Kelurahan <span style="color: red">*</span></label>
            <div class="col-sm-10">
            @if (isset($siswa))
            <input type="text" required name="kelurahan" class="form-control" id="" value="{{ $siswa->kelurahan }}">
            @else
            <select name="kelurahan" required id="kelurahan" class="form-control select2_tags">
                <option value="">Pilih Kelurahan</option>
            </select>
            @endif
            </div>
        </div>
        <div class="nama_lokasi"></div>
        <div class="form-group">
            <label for="" class="col-sm-2 text-right">Alamat <span style="color: red">*</span></label>
            <div class="col-sm-10">
            <textarea name="alamat" required id="" cols="30" rows="3" class="form-control" placeholder="Isikan nama jalan / dusun / RT/RW ">{{ isset($siswa) ? $siswa->alamat : '' }}</textarea>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 text-right">Transportasi <span style="color: red">*</span></label>
            <div class="col-sm-10">
            <select name="transportasi" required class="form-control" id="">
                <option @if(isset($siswa) && $siswa->transportasi == 'Jalan Kaki') selected @endif>Jalan Kaki</option>
                <option @if(isset($siswa) && $siswa->transportasi == 'Sepeda Motor') selected @endif>Sepeda Motor</option>
                <option @if(isset($siswa) && $siswa->transportasi == 'Angkutan Umum') selected @endif>Angkutan Umum</option>
            </select>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 text-right">No WA <span style="color: red">*</span></label>
            <div class="col-sm-10">
                <input required type="text" name="no_hp" id="" class="form-control" placeholder="085123123123" value="{{ isset($siswa) ? $siswa->no_hp : '' }}">
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 text-right">Jenis Bantuan</label>
            <div class="col-sm-10">
                <input type="checkbox" name="jenis_bantuan" value="KIP" @if(isset($siswa) && $siswa->jenis_bantuan == 'KIP') checked @endif> KIP
                <input type="checkbox" name="jenis_bantuan" value="KPS" @if(isset($siswa) && $siswa->jenis_bantuan == 'KPS') checked @endif> KPS
                <input type="checkbox" name="jenis_bantuan" value="BLT" @if(isset($siswa) && $siswa->jenis_bantuan == 'BLT') checked @endif> BLT
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 text-right">No Kartu Bantuan </label>
            <div class="col-sm-10">
            <input type="text" name="no_bantuan" class="form-control" id="" value="{{ isset($siswa) ? $siswa->no_bantuan : '' }}">
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 text-right">Lintang, Bujur </label>
            <div class="col-sm-10">
            <div class="form-inline">
                <input type="number" name="lintang" class="form-control" value="{{ isset($siswa) ? $siswa->lintang : '' }}" placeholder="Lintang">
                <input type="number" name="bujur" class="form-control" value="{{ isset($siswa) ? $siswa->bujur : '' }}" placeholder="Bujur">
            </div>
            </div>
        </div>

        </div>

        <div class="box-footer">
        {{-- <button type="submit" class="btn btn-primary pull-right">Simpan</button> --}}
        </div>
        </div>
    {{-- </form> --}}
</div>
