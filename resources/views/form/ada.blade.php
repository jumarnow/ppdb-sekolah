@extends('layouts.siswa')
@section('content')
<section class="content">
    {{-- <div class="callout callout-info">
        <h4>
            Formulir telah terkirim, silahkan cetak formulir <a href="{{ url('cetak_form', $data[0]->id) }}">Disini</a> 
            kemudian bawa ke sekolahan saat membayar pendaftaran
        </h4>
    </div> --}}

    <div class="row">
        <div class="col-md-6">
          <div class="box box-default">
            <div class="box-header with-border">
              <i class="fa fa-bullhorn"></i>

              <h3 class="box-title">Informasi</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Formulir telah terkirim!</h4>
                Silahkan mengikuti prosedur pendaftaran berikut
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->

        <div class="col-md-6">
          <div class="box box-default">
            <div class="box-header with-border">
              <i class="fa fa-bullhorn"></i>

              <h3 class="box-title">Prosedur Pendaftaran</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              1. Download formulir pendaftaran di sini <a target="blank" href="{{ url('cetak_form') }}/{{ $siswa->id ?? '' }}" class="btn btn-success btn-sm"><i class="fa fa-download"></i> Download</a> <br>
              2. Tunjukan formulir tersebut kepada panitian PPDB SMP Futuhiyyah Mranggen <br>
              3. Membayar biaya pendaftaran <br>
              4. Pengumuman lolos seleksi akan diumumkan melalui no Wa yang terdaftar <br>
              5. Melengkapi berkas dan membayar biaya daftar ulang <br> <br>

              Contact Person : <br>
              Ibu Sa'adah <br>
              <a href="http://wa.me/6281238423174">wa.me/6281238423174</a>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>

</div>


</section>
@endsection