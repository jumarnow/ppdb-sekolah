<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Data Keluarga</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    {{-- <form role="form" action="{{ route('data_keluarga.store') }}"> --}}
        <div class="form-horizontal">
        <div class="box-body">
        <div class="form-group">
            <label for="" class="col-sm-2 text-right">Nama Ayah <span style="color: red">*</span></label>
            <div class="col-sm-10">
            <input type="text" required class="form-control" name="nama_ayah" value="{{ isset($siswa) ? $siswa->keluarga->nama_ayah : '' }}">
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 text-right">Pekerjaan Ayah <span style="color: red">*</span> </label>
            <div class="col-sm-10">
            <input type="text" required class="form-control" name="pekerjaan_ayah" value="{{ isset($siswa) ? $siswa->keluarga->pekerjaan_ayah : '' }}">
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 text-right">Pendidikan Ayah <span style="color: red">*</span> </label>
            <div class="col-sm-10">
            <select name="pendidikan_ayah" required class="form-control" id="">
                <option value="">**Pilih Pendidikan</option>
                <option @if(isset($siswa) && $siswa->keluarga->pendidikan_ayah == 'Tidak Sekolah') selected @endif>Tidak Sekolah</option>
                <option @if(isset($siswa) && $siswa->keluarga->pendidikan_ayah == 'SD') selected @endif>SD</option>
                <option @if(isset($siswa) && $siswa->keluarga->pendidikan_ayah == 'SLTP') selected @endif>SLTP</option>
                <option @if(isset($siswa) && $siswa->keluarga->pendidikan_ayah == 'SLTA') selected @endif>SLTA</option>
                <option @if(isset($siswa) && $siswa->keluarga->pendidikan_ayah == 'D3') selected @endif>D3</option>
                <option @if(isset($siswa) && $siswa->keluarga->pendidikan_ayah == 'S1') selected @endif>S1</option>
                <option @if(isset($siswa) && $siswa->keluarga->pendidikan_ayah == 'S2') selected @endif>S2</option>
                <option @if(isset($siswa) && $siswa->keluarga->pendidikan_ayah == 'S3') selected @endif>S3</option>
            </select>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 text-right">Penghasilam Ayah </label>
            <div class="col-sm-10">
            <select name="penghasilan_ayah" class="form-control" id="">
                <option value="">**Pilih Penghasilan</option>
                <option @if(isset($siswa) && $siswa->keluarga->penghasilan_ayah == 'Tidak Berpenghasilan') selected @endif>Tidak Berpenghasilan</option>
                <option @if(isset($siswa) && $siswa->keluarga->penghasilan_ayah == '< 500.000') selected @endif>< 500.000</option>
                <option @if(isset($siswa) && $siswa->keluarga->penghasilan_ayah == '500.000 - 1.000.000') selected @endif>500.000 - 1.000.000</option>
                <option @if(isset($siswa) && $siswa->keluarga->penghasilan_ayah == '1.000.000 - 3.000.000') selected @endif>1.000.000 - 3.000.000</option>
                <option @if(isset($siswa) && $siswa->keluarga->penghasilan_ayah == '3.000.000 - 5.000.000') selected @endif>3.000.000 - 5.000.000</option>
                <option @if(isset($siswa) && $siswa->keluarga->penghasilan_ayah == '> 5.000.000') selected @endif>> 5.000.000</option>
            </select>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 text-right">Nama Ibu <span style="color: red">*</span></label>
            <div class="col-sm-10">
            <input type="text" required class="form-control" name="nama_ibu" value="{{ isset($siswa) ? $siswa->keluarga->nama_ibu : '' }}">
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 text-right">Pekerjaan Ibu <span style="color: red">*</span></label>
            <div class="col-sm-10">
            <input type="text" required class="form-control" name="pekerjaan_ibu" value="{{ isset($siswa) ? $siswa->keluarga->pekerjaan_ibu : '' }}">
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 text-right">Pendidikan Ibu <span style="color: red">*</span></label>
            <div class="col-sm-10">
            <select name="pendidikan_ibu" required class="form-control" id="">
                <option value="">**Pilih Pendidikan</option>
                <option @if(isset($siswa) && $siswa->keluarga->pendidikan_ibu == 'Tidak Sekolah') selected @endif>Tidak Sekolah</option>
                <option @if(isset($siswa) && $siswa->keluarga->pendidikan_ibu == 'SD') selected @endif>SD</option>
                <option @if(isset($siswa) && $siswa->keluarga->pendidikan_ibu == 'SLTP') selected @endif>SLTP</option>
                <option @if(isset($siswa) && $siswa->keluarga->pendidikan_ibu == 'SLTA') selected @endif>SLTA</option>
                <option @if(isset($siswa) && $siswa->keluarga->pendidikan_ibu == 'D3') selected @endif>D3</option>
                <option @if(isset($siswa) && $siswa->keluarga->pendidikan_ibu == 'S1') selected @endif>S1</option>
                <option @if(isset($siswa) && $siswa->keluarga->pendidikan_ibu == 'S2') selected @endif>S2</option>
                <option @if(isset($siswa) && $siswa->keluarga->pendidikan_ibu == 'S3') selected @endif>S3</option>
            </select>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 text-right">Penghasilam Ibu </label>
            <div class="col-sm-10">
            <select name="penghasilan_ibu" class="form-control" id="">
                <option value="">**Pilih Penghasilan</option>
                <option @if(isset($siswa) && $siswa->keluarga->penghasilan_ibu == 'Tidak Berpenghasilan') selected @endif>Tidak Berpenghasilan</option>
                <option @if(isset($siswa) && $siswa->keluarga->penghasilan_ibu == '< 500.000') selected @endif>< 500.000</option>
                <option @if(isset($siswa) && $siswa->keluarga->penghasilan_ibu == '500.000 - 1.000.000') selected @endif>500.000 - 1.000.000</option>
                <option @if(isset($siswa) && $siswa->keluarga->penghasilan_ibu == '1.000.000 - 3.000.000') selected @endif>1.000.000 - 3.000.000</option>
                <option @if(isset($siswa) && $siswa->keluarga->penghasilan_ibu == '3.000.000 - 5.000.000') selected @endif>3.000.000 - 5.000.000</option>
                <option @if(isset($siswa) && $siswa->keluarga->penghasilan_ibu == '> 5.000.000') selected @endif>> 5.000.000</option>
            </select>
            </div>
        </div>


        </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
        {{-- <button type="submit" class="btn btn-primary">Simpan</button> --}}
        </div>
    {{-- </form> --}}
</div>
