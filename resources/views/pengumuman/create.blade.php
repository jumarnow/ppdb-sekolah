@extends('layouts.admin')
@section('content')

<section class="content-header">
    <h1>
        <i class='fa fa-users'></i> {{isset($pengumuman) ? 'Edit' : 'Tambah'}} Master Pengumuman &nbsp;&nbsp;
    </h1>

    <ol class="breadcrumb">
        <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Halaman Awal</a></li>
        <li class="active">Master Pengumuman</li>
    </ol>
</section>

<!-- Main content -->
<section id='content_section' class="content">
    
@if (isset($pengumuman))
<form class='form-horizontal' method='post' id="form" enctype="multipart/form-data" action="{{route('pengumuman.update', $pengumuman->id)}}">
@method('PATCH')
@else
<form class='form-horizontal' method='post' id="form" enctype="multipart/form-data" action="{{route('pengumuman.store')}}">
@endif
@csrf

<div class="panel panel-default" style="margin-bottom: 10px">
    <div class="panel-heading">
        {{-- <strong><i class='fa fa-users'></i> Tambah Master Pengumuman</strong> --}}
    </div>
    <div class="panel-body" style="padding:20px 0px 0px 0px">
        <div class="box-body" id="parent-form-area">

            <div class='form-group header-group-0'>
                <label class='control-label col-sm-2'> Judul <span class='text-danger' title='This field is required'>*</span></label>
                <div class="col-sm-10">
                    <input type='text' title="Pesan" required class='form-control' name="judul" id="" value='{{isset($pengumuman) ? $pengumuman->judul : old('judul')}}'/>
                    <div class="text-danger"></div>
                    <p class='help-block'></p>
                </div>
            </div>

            <div class='form-group header-group-0'>
                <label class='control-label col-sm-2'> Deskripsi <span class='text-danger' title='This field is required'>*</span></label>
                <div class="col-sm-10">
                    <textarea name="isi" class="form-control" id="editor" cols="30" rows="3">{{isset($pengumuman) ? $pengumuman->isi : old('isi')}}</textarea>
                    <div class="text-danger"></div>
                    <p class='help-block'></p>
                </div>
            </div>
            
        </div><!-- /.box-body -->
    </div>
</div>

<div class="row">
    <div class="col-sm-6"></div>
    <div class="col-sm-6">
        <div align="right">
            <input type="submit" name="submit" value='Simpan' class='btn btn-success'>
        </div>
    </div>
</div>

</form>
</div><!--END AUTO MARGIN-->

</section><!-- /.content -->
@endsection
@section('js')
<script>

	CKEDITOR.replace('editor')
</script>
@endsection