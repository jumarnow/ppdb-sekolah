@extends('layouts.siswa')
@section('content')
<section class="content-header">
    <h1>
    Pengumuman
    </h1>
    <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Pengumuman</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <ul class="timeline">
            @foreach ($pengumuman as $item)
            <li>
                <i class="fa fa-envelope bg-blue"></i>
                <div class="timeline-item">
                <span class="time"><i class="fa fa-clock-o"></i> {{ $item->created_at }}</span>
                <h3 class="timeline-header">{{ $item->judul }} <small>By Admin</small> </h3> 
                <div class="timeline-body">
                {!! $item->isi !!}
                </div>
                </div>
            </li>
            @endforeach
            </ul>
        </div>
    </div>
</section>
@endsection

