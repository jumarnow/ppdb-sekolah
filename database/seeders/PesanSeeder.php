<?php

namespace Database\Seeders;

use App\Models\Pesan;
use Illuminate\Database\Seeder;

class PesanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $data = new Pesan();
        $data->id = 1; 
        $data->isi = 'Berdasarkan keputusan Panitia Peserta Didik Baru SMP FUTUHIYYAH Mranggen Demak,dengan ini memutuskan bahwa: %0D%0A%0D%0A Nama :  {{ $data->nama_lengkap }} %0D%0A Asal Sekolah :  {{ $data->nama_sekolah_asal }} %0D%0A==*DITERIMA*== %0D%0A%0D%0A Sebagai siswa/siswi SMP FUTUHIYYAH Mranggen Demak pada tahun pelajaran 2022/2023. Selanjutnya dimohon segera melakukan daftar ulang dan melengkapi berkas pendaftaran.';
        $data->save();
    }
}
