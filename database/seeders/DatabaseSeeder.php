<?php

namespace Database\Seeders;

use App\Models\DataLain;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // User::factory(10)->create();
        $this->call([
            DataPesertaDidikSeeder::class,
            DataKeluargaSeeder::class,
            DataLainSeeder::class,
            InfomasiSeeder::class,
            PesanSeeder::class,
        ]);
        
        $user = new User();
        $user->name = 'Admin';
        $user->username = 'admin';
        $user->password = Hash::make('smpfgreen');
        $user->roles = 'Admin';
        $user->save();
    }
}
