<?php

namespace App\Http\Controllers;

use App\Models\DataKeluarga;
use App\Models\DataLain;
use App\Models\DataPesertaDidik;
use Illuminate\Http\Request;
use PDF;
use Illuminate\Support\Facades\DB;

class FormController extends Controller
{
    //
    public function index(){
        return view('form.form_index');
    }
    public function store(Request $request){
        // dd($request->all());
        // DB::beginTransaction();
        $data = DataPesertaDidik::where('nisn', $request->nisn)->get();
        if ($data->count() > 0) {
            $siswa = $data[0];
            return view('form.ada', compact('siswa'));
        }else{
            try {
                $siswa = new DataPesertaDidik();
                $siswa->nama_lengkap = $request->nama_lengkap;
                $siswa->jenis_kelamin = $request->jenis_kelamin;
                $siswa->nisn = $request->nisn;
                $siswa->nik = $request->nik;
                $siswa->nama_sekolah_asal = $request->nama_sekolah_asal;
                $siswa->alamat_sekolah_asal = $request->alamat_sekolah_asal;
                $siswa->tempat_lahir = $request->tempat_lahir;
                $siswa->tanggal_lahir = $request->tanggal_lahir;
                $siswa->agama = $request->agama;
                $siswa->provinsi = $request->provinsi;
                $siswa->kabupaten = $request->kabupaten;
                $siswa->kecamatan = $request->kecamatan;
                $siswa->kelurahan = $request->kelurahan;
                $siswa->alamat = $request->alamat;
                $siswa->transportasi = $request->transportasi;
                $siswa->no_hp = $request->no_hp;
                $siswa->jenis_bantuan = $request->jenis_bantuan;
                $siswa->no_bantuan = $request->no_bantuan;
                $siswa->lintang = $request->lintang;
                $siswa->bujur = $request->bujur;
                $siswa->save();

                $keluarga = new DataKeluarga();
                $keluarga->nama_ayah = $request->nama_ayah;
                $keluarga->pekerjaan_ayah = $request->pekerjaan_ayah;
                $keluarga->pendidikan_ayah = $request->pendidikan_ayah;
                $keluarga->penghasilan_ayah = $request->penghasilan_ayah;
                $keluarga->nama_ibu = $request->nama_ibu;
                $keluarga->pekerjaan_ibu = $request->pekerjaan_ibu;
                $keluarga->pendidikan_ibu = $request->pendidikan_ibu;
                $keluarga->penghasilan_ibu = $request->penghasilan_ibu;
                $keluarga->siswa_id = $siswa->id;
                $keluarga->save();

                $lain = new DataLain();
                $lain->nama_pondok = $request->nama_pondok;
                $lain->tinggi_badan = $request->tinggi_badan;
                $lain->berat_badan = $request->berat_badan;
                $lain->jarak_kesekolah = $request->jarak_kesekolah;
                $lain->waktu_kesekolah = $request->waktu_kesekolah;
                $lain->anak_ke = $request->anak_ke;
                $lain->jumlah_saudara = $request->jumlah_saudara;
                $lain->siswa_id = $siswa->id;
                $lain->save();

                $siswa = DataPesertaDidik::find($siswa->id);
                // dd($siswa);

                return view('form.ada', compact('siswa'));

                // $pdf = PDF::loadView('form.pdf', compact('siswa'));
                // return $pdf->stream($siswa->nama_lengkap.'-formulir-pendaftaran.pdf');
                // DB::commit();
            } catch (\Throwable $th) {
                // DB::rollBack();
                return back()->with('error', $th->getMessage());
            }

        }
    }

    public function cetak_form($id){
        $siswa = DataPesertaDidik::find($id);
        $pdf = PDF::loadView('form.pdf', compact('siswa'));
        return $pdf->stream($siswa->nama_lengkap.'-formulir-pendaftaran.pdf');
    }

    public function detil($id){
        $siswa = DataPesertaDidik::find($id);
        return view('detil', compact('siswa'));
    }

    public function edit($id){
        $siswa = DataPesertaDidik::find($id);
        return view('edit', compact('siswa'));
    }

    public function simpan_data_pendaftar(Request $request){
        // dd($request->all());
        try {
            $siswa = DataPesertaDidik::whereNisn($request->nisn)->first();
            $siswa->nama_lengkap = $request->nama_lengkap;
            $siswa->jenis_kelamin = $request->jenis_kelamin;
            $siswa->nisn = $request->nisn;
            $siswa->nik = $request->nik;
            $siswa->nama_sekolah_asal = $request->nama_sekolah_asal;
            $siswa->alamat_sekolah_asal = $request->alamat_sekolah_asal;
            $siswa->tempat_lahir = $request->tempat_lahir;
            $siswa->tanggal_lahir = $request->tanggal_lahir;
            $siswa->agama = $request->agama;
            $siswa->provinsi = $request->provinsi;
            $siswa->kabupaten = $request->kabupaten;
            $siswa->kecamatan = $request->kecamatan;
            $siswa->kelurahan = $request->kelurahan;
            $siswa->alamat = $request->alamat;
            $siswa->transportasi = $request->transportasi;
            $siswa->no_hp = $request->no_hp;
            $siswa->jenis_bantuan = $request->jenis_bantuan;
            $siswa->no_bantuan = $request->no_bantuan;
            $siswa->lintang = $request->lintang;
            $siswa->bujur = $request->bujur;
            $siswa->save();

            $keluarga = DataKeluarga::whereSiswaId($siswa->id)->first();
            $keluarga->nama_ayah = $request->nama_ayah;
            $keluarga->pekerjaan_ayah = $request->pekerjaan_ayah;
            $keluarga->pendidikan_ayah = $request->pendidikan_ayah;
            $keluarga->penghasilan_ayah = $request->penghasilan_ayah;
            $keluarga->nama_ibu = $request->nama_ibu;
            $keluarga->pekerjaan_ibu = $request->pekerjaan_ibu;
            $keluarga->pendidikan_ibu = $request->pendidikan_ibu;
            $keluarga->penghasilan_ibu = $request->penghasilan_ibu;
            $keluarga->siswa_id = $siswa->id;
            $keluarga->save();

            $lain = DataLain::whereSiswaId($siswa->id)->first();
            $lain->nama_pondok = $request->nama_pondok;
            $lain->tinggi_badan = $request->tinggi_badan;
            $lain->berat_badan = $request->berat_badan;
            $lain->jarak_kesekolah = $request->jarak_kesekolah;
            $lain->waktu_kesekolah = $request->waktu_kesekolah;
            $lain->anak_ke = $request->anak_ke;
            $lain->jumlah_saudara = $request->jumlah_saudara;
            $lain->siswa_id = $siswa->id;
            $lain->save();
            return back()->with('success', 'Data berhasil disimpan');
        } catch (\Throwable $th) {
            dd($th->getMessage());
        }
    }
}
