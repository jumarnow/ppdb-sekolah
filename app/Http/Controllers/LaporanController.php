<?php

namespace App\Http\Controllers;

use App\Models\DataPesertaDidik;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\GlobalExport;

class LaporanController extends Controller
{
    //
    public function export_data(Request $request){
        $data_siswa = DataPesertaDidik::all();
        $siswa = DataPesertaDidik::select("*");
        if ($request->nama) {
            $siswa = $siswa->where('nama_lengkap', 'like', '%'.$request->nama.'%');
        }
        if ($request->bayar == 'pendaftaran') {
            $siswa = $siswa->whereNotNull('pendaftaran');
        }
        if ($request->bayar == 'daftar_ulang') {
            $siswa = $siswa->whereNotNull('daftar_ulang');
        }
        $siswa = $siswa->orderBy('created_at','desc')->get();
        // dd($siswa);
        $return = view('export.export_data', compact('siswa'));

        return Excel::download(new GlobalExport($return), 'Data siswa PPDB Per '.Carbon::now()->isoFormat('D MMMM Y').'.xlsx');

    }
}
